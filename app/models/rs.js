var mongoose = require('mongoose'),
	Schema = mongoose.Schema;


var RsSchema = new Schema({
	name: {
		type: String,
		unique: true,
		required:true
	},
	jenis :{
		type :String,
		required : true
	},
	address:{
		province: {
			type: String,
			required:true		
		},
		city:{
			type:String,
			required:true
		},
		detail: {
			type: String,
			required:true
		}
	},
	tanggal : {
		type : String,
		required: true
	}
});

mongoose.model('rumahsakit', RsSchema);