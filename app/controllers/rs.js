var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  rs = mongoose.model('rumahsakit'),
  Article = mongoose.model('Article');
  cors = require('cors');
module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};
router.post('/input', function(req, res, next){
  var data = rs.findOne({name:req.body.name});
  data.exec(function(err, result){
    if(result == null){
      var inputdata = new rs({name:req.body.name,jenis: req.body.jenis,
                      address:req.body.address, tanggal: req.body.tanggal});
      inputdata.save(function(err){
        if(err)
          console.log(err);
        else {
          console.log('input rumah sakit sukses');
          res.json({
            status:"success",
            message:"Selamat anda telah berhasil menambahkan satu data rumah sakit kedalam database"
          }).status(200)
        }
      })
    } else {
      res.status(409).json({message:'rumah sakit sudah ada'})
    }
  })
});

router.get('/rumahsakit', function(req, res, next){
  var data = rs.find({});
  data.exec(function(err, result){
    if (err)
      return handleError(err);
    res.json({result}).status(200);
  })
});

router.get('/rumahsakit/:id', function(req, res, next){
  var data = rs.findOne({"_id":req.params.id});
  data.exec(function(err, result){
    if(err)
      return handleError(err);
    res.json({result}).status(200);
  })
});


router.put('/rumahsakit/:id', function(req, res, next){
  var data = rs.findOne({"_id":req.params.id});
  data.exec(function(err, result){
    if (result == null){
      res.status(404).json({message:"not found"});
    } else {
      var dataold = {'_id': req.params.id};
      var datanew = {$set:req.body};
      var options = {};
      rs.update(dataold, datanew, options, callback);
      function callback(err, numAffected){
        res.json({message:"update sukses"}).status(200);
      }
    }
  })
})

router.post('/rumahsakit/:id', function(req, res, next){
  var data = rs.findOne({"_id":req.params.id});
  data.exec(function(err, result){
    if (result == null){
      res.status(404).json({message:"not found"});
    } else {
      var dataold = {'_id': req.params.id};
      var datanew = {$set:req.body};
      var options = {};
      rs.update(dataold, datanew, options, callback);
      function callback(err, numAffected){
        res.json({message:"update sukses"}).status(200);
      }
    }
  })
})

router.delete('/rumahsakit/:id' ,function(req,res,next){
  var data = rs.findOne({'_id':req.params.id});
  data.exec(function(err, result){
    if (result==null) {
      res.status(404).json({message :"not found"});
    } else {
      var data = rs.remove({'_id':req.params.id});
      data.exec(function(err, result){
        if (err) {
          console.log(err);
        } else {
          res.json({message : "data terhapus"}).status(404);
        }
      })
    }
  })
})