const saltRound = 10;
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var ls = require('local-storage');
var express = require('express'),
	router = express.Router(),
	app = express(),
	mongoose = require('mongoose'),
	user = mongoose.model('User');

module.exports = function(app) {
	app.use('/v2', router);
}

router.post('/register', function(req,res,next){
	var data = user.findOne({username:req.body.username});
	data.exec(function(err, result){
		if (result == null) {
			bcrypt.hash(req.body.password, saltRound, function(err, hash){
				var data = new user({
					fullname:req.body.fullname,
					email	:req.body.email,
					username:req.body.username,
					password:hash
				});
				data.save(function(err){
					if (err) {
						console.log(err)
					}
				});
				res.json({message:"berhasil"})
			});
		} else {
			res.json({message:"username sudah ada"})
		}
	})
})

router.get('/user', function(req,res,next){
	var data = user.find({});
	data.exec(function(err, result){
		if (err) return handleError(err);
		res.json({result}).status(200);
	})
});

app.set('superSecret', ' ');
router.post('/login', function(req,res,next){
	var data = user.findOne({username:req.body.username});
	data.exec(function(err,result){
		if (result == null) {
			res.status(401).json({message: "Username tidak terdaftar"});
		} else {
			bcrypt.compare(req.body.password, result.password, function(err, compare){
				if (compare ==true) {
					var token = jwt.sign(result, app.get('superSecret'),{
											expiresIn: 60*60
										});
					ls.set('token', token);
					res.json({message: " login berhasil",
							token: token}).status(200);
				} else {
					res.status(409).json({message:"Password salah"});
				}
			});
		}
	})
});

router.post('/logout', function(req, res, next){
	var decoded = jwt.decode(ls.get('token'));
	if (decoded == null) {
		res.json({message : "token tidak ditemukan"}).status(404);
	} else {
		var id_username = decoded.$__.id;
		var data = user.findOne({_id: id_username});
		data.exec(function(err, result){
			if (result == null) {
				res.json({message : "logout tidak ditemukan"}).status(404);
			} else {
				res.json({message: "logout gagal"}).status(200);
			}
		});
	};
	ls.clear();
});

router.get('/checktoken/:token', function(req, res, next){
  var decoded = jwt.decode(req.params.token);
  result = {
    uid:decoded._doc._id,
    username:decoded._doc.username
  }
  res.json({decoded});
});


