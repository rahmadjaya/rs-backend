var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

UserSchema = new Schema({
	fullname : String,
	email	 : String,
	username : String,
	password : String
})

mongoose.model('User', UserSchema);